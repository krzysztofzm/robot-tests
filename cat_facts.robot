***Settings ***
Documentation    main test set file
Library    SeleniumLibrary
Library    Resources/APILib.py
Resource    Resources/Variables.robot
Resource    Resources/Rentry_main_page.robot
Resource    Resources/Rentry_published.robot

***Test Cases ***
Get cat facts
    ${amount}=    Random Number
    ${CatFacts}=    Get Cat Facts    ${amount}
    Log    ${CatFacts}    console=True
    Open Browser    ${StartPage}    Chrome
    Page Should Contain Element    ${TextFieldLocator}
    Press Keys    ${TextFieldLocator}    ${CatFacts}
    Click Button    ${SubmitButtonLocator}
    Wait Until Element Is Visible    ${PublishTextLocator}
    ${PublishedText}=    Get Text    ${PublishTextLocator}
    Log    ${PublishedText}    console=True
    ${LinkToPublisedDoc}=    Get Element Attribute    ${LinkToPageLocator}    href
    ${LogLink}=    Catenate    The link to the document is:    ${LinkToPublisedDoc}
    Log    ${LogLink}    console=True
    Should Be Equal As Strings    ${CatFacts.strip()}    ${PublishedText.strip()}
    [Teardown]    Close Browser
