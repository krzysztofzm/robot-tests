***Settings ***
Documentation   Page file for Rentry main page.
...             contains page specific locators, variables, keywords (if applicable:-))

***Variables ***
${TextFieldLocator}=    //span[@role="presentation"]
${SubmitButtonLocator}=    //button[@id="submitButton"]