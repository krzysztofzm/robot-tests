***Settings ***
Documentation   Global variables and keywords file
Library    RequestsLibrary

***Variables ***
${StartPage}=    https://rentry.co

***Keywords ***
Random Number
    [Documentation]    returs a random number from defined range using http://www.randomnumberapi.com/ api
    [Arguments]    ${min}=1    ${max}=10
    ${response}=    GET  http://www.randomnumberapi.com/api/v1.0/random    params=min=${min}&max=${max}&count=1    expected_status=200
    [Return]    ${response.json()}[0]
