***Settings ***
Documentation   Page file for Rentry after publishing a document
...             contains page specific locators, variables, keywords (if applicable:-))

***Variables ***
${PublishTextLocator}=    //article//p
${LinkToPageLocator}=    //link[@rel="canonical"]
