import requests


def get_cat_facts(cat_facts_number):
    """
    returns a given number of cat facts using https://cat-fact.herokuapp.com/facts/random api
    """
    cat_facts = ""
    payload = {'animal_type': 'cat', 'amount': cat_facts_number}
    response = requests.get('https://cat-fact.herokuapp.com/facts/random', params=payload)
    response.raise_for_status()
    if cat_facts_number == 1:
        cat_facts += (response.json()["text"]) + "\n"
    else:
        for i in range(cat_facts_number):
            cat_facts += (response.json()[i]["text"]) + "\n"
    return cat_facts
